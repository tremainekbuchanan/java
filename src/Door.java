/* 
Author: Tremaine Buchanan
Date Created : March 2013
Class: Door - implementing functionality of a door
*/
public class Door
{
	int height, weight;
	boolean open;
	String color;


/**
Basic constructor for Door
*/
Door()
{
	height = 0;
	weight = 0;
	open = false;
	color = null;
}

public void closeDoor()
{
	open = false;
}

public void openDoor()
{
	open = true;

}

public boolean isOpen()
{
	return open;

}

public void setHeight(int height)
{
	this.height = height; 
}

public void setWeight(int weight)
{
	this.weight = weight; 
}

public void setColor(String color)
{
	this.color = color; 
}

public int getHeight()
{
	return height;
}

public int getWeight()
{
	return weight;
}

public String getColor()
{
	return color;
}

}
