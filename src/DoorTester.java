
public class DoorTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Door d = new Door();
		
		d.setColor("Red");
		d.setHeight(10);
		d.setWeight(10);
		
			
		//System.out.println(d.isOpen());	
		System.out.println("Color of door " + d.getColor());
		System.out.println("Weight of door: " + d.getWeight());
		System.out.println("Height of door: " + d.getHeight());
		
		d.openDoor();
		
		if(d.isOpen())
		{
			System.out.println("Door is open\n");
		}
		else
			System.out.println("Door closed");
		
		
		

	}

}
